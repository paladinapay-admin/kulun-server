from django.contrib.auth import get_user_model
from rest_framework import serializers
from models import Device


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('email', 'first_name', 'last_name', 'phone', 'address', 'credit', 'devices', 'pictures')


# class DeviceSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Device
#         fields = ('deviceID', 'btID', 'activeCode')


class DeviceRegisterSerializer(serializers.Serializer):
    deviceID = serializers.CharField(max_length=10)
    btID = serializers.CharField(max_length=10)
    activeCode = serializers.CharField(max_length=10)