from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from serializers import UserSerializer, DeviceRegisterSerializer
import time
from models import Device


class Time(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        content = {'time': str(time.time())}
        return Response(content, status=status.HTTP_200_OK)


class UserMe(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = UserSerializer

    def get(self, request, format=None):
        user = request.user

        if user.email == request.GET.get('user', ''):
            return Response(self.serializer_class(user).data)
        else:
            # token & user are not matched, the way to avoid multiple users having same tokens
            return Response({'error': 'invalid request'}, status=status.HTTP_400_BAD_REQUEST)


class RegisterDevice(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = DeviceRegisterSerializer

    def post(self, request, format=None):

        serializer = self.serializer_class(data=request.DATA)

        if serializer.is_valid():
            bt_id = serializer.data['btID']
            device_id = serializer.data['deviceID']
            active_code = serializer.data['activeCode']

            user = request.user

            # user as a url parameter
            if user.email == request.GET.get('user', ''):
                try:
                    device = Device.objects.get(btID=bt_id, deviceID=device_id, activeCode=active_code)
                    owner = device.owner

                    # device can be registered only if the current owner is an administrator
                    if owner.is_staff and owner.is_superuser:
                        device.owner = user
                        device.save()
                        return Response({'owner': user.email}, status=status.HTTP_200_OK)
                    else:
                        # has already been registered by another non-admin user
                        return Response({'error': 'the device has been registered'}, status=status.HTTP_403_FORBIDDEN)
                except Device.DoesNotExist:
                    # no matched device is found
                    return Response({'error': 'no matched record is found'}, status=status.HTTP_404_NOT_FOUND)
            else:
                # token & user are not matched, the way to avoid multiple users having same tokens
                return Response({'error': 'invalid request'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
