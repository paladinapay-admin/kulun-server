from django.contrib import admin
from django.contrib.auth import get_user_model
from authemail.admin import EmailUserAdmin
from models import Device, Picture, MyUser


class DeviceInline(admin.TabularInline):
    model = Device
    fk_name = "owner"
    max_num = 0
    can_delete = False

    def get_readonly_fields(self, request, obj=None):
        fields = ['deviceID', 'btID', 'activeCode']
        return fields

    def has_add_permission(self, request):
        return False


class PictureInline(admin.TabularInline):
    model = MyUser.pictures.through


class MyUserAdmin(EmailUserAdmin):

    inlines = [DeviceInline, PictureInline]
    inlines = inlines + EmailUserAdmin.inlines

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal Info', {'fields': ('first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_active', 'is_staff',
                                    'is_superuser', 'is_verified',
                                    'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Custom info', {'fields': ('phone', 'address', 'credit')}),
    )


class DeviceAdmin(admin.ModelAdmin):

    fields = ('deviceID', 'btID', 'activeCode', 'owner')


class PictureAdmin(admin.ModelAdmin):
    inlines = [PictureInline]
    fields = ('type', 'imgLocation', 'dzkLocation')

admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), MyUserAdmin)
admin.site.register(Device, DeviceAdmin)
admin.site.register(Picture, PictureAdmin)