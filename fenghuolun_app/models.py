from django.db import models
from authemail.models import EmailAbstractUser, EmailUserManager, timezone
from fenghuolun_server import settings


# picture table
# TODO optimise
# TODO dzkLocation to dzk
class Picture(models.Model):
    type = models.CharField(max_length=20)
    imgLocation = models.URLField(unique=True)
    dzkLocation = models.URLField(unique=True)


# TODO update length of each field
class MyUser(EmailAbstractUser):
    # Custom fields
    phone = models.CharField(max_length=20, blank=True)
    address = models.CharField(max_length=100, blank=True)
    credit = models.PositiveIntegerField(null=True, default=0)
    pictures = models.ManyToManyField(Picture)

    # Required
    REQUIRED_FIELDS = ['first_name', 'last_name', 'phone', 'address']
    objects = EmailUserManager()


# device table
# TODO decide max_length for deviceID, btID and activeCode
# TODO ensure if deviceID & btID are both unique
# TODO optimise
class Device(models.Model):
    deviceID = models.CharField(unique=True, max_length=10)
    btID = models.CharField(unique=True, max_length=10)
    activeCode = models.CharField(max_length=10)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL,
                              related_name='devices',
                              related_query_name='devices')