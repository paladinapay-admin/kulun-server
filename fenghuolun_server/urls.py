from django.conf.urls import patterns, include, url
from django.contrib import admin
from fenghuolun_app import views
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^kulunadmin/', include(admin.site.urls)),

    url(r'', include('authemail.urls')),

    url(r'^time/', views.Time.as_view(), name='time'),

    url(r'^user/me/$', views.UserMe.as_view(), name='me'),

    url(r'^user/device/register/$', views.RegisterDevice.as_view(), name='device-register'),
)