from django.contrib.auth import authenticate, get_user_model
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from authemail.models import SignupCode, PasswordResetCode, timezone
from authemail.serializers import TempPasswordSerializer, LoginSerializer
from authemail.serializers import PasswordResetSerializer
from authemail.serializers import PasswordResetVerifiedSerializer
from authemail.serializers import PasswordChangeSerializer
import random


def _generate_temp_password():
    s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%&*"
    password = random.choice(s)
    for x in range(0, 6):
        password += random.choice(s)
    return password
#
#
# def _generate_temp_password1():
#     return binascii.hexlify(os.urandom(3))


class Signup(APIView):
    permission_classes = (AllowAny,)
    serializer_class = TempPasswordSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.DATA)

        if serializer.is_valid():
            email = serializer.data['email']
            # password = serializer.data['password']
            # first_name = serializer.data['first_name']
            # last_name = serializer.data['last_name']
            # phone = serializer.data['phone']
            # address = serializer.data['address']

            try:
                user = get_user_model().objects.get(email=email)
                if user.is_verified:
                    content = {'detail': 
                        'User with this Email address already exists.'}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)

                # try:
                #     # Delete old signup codes
                #     signup_code = SignupCode.objects.get(user=user)
                #     signup_code.delete()
                # except SignupCode.DoesNotExist:
                #     pass
                    
            except get_user_model().DoesNotExist:
                user = get_user_model().objects.create_user(email=email)

            tokens = Token.objects.filter(user=user)
            for token in tokens:
                token.delete()

            code = _generate_temp_password()

            # Set user fields provided
            user.set_password(code)
            #user.is_verified = True
            # user.first_name = first_name
            # user.last_name = last_name
            # user.phone = phone
            # user.address = address
            user.save()

            user.send_email('temp_password_email', code)

            # Create and associate signup code
            #ipaddr = self.request.META.get('REMOTE_ADDR', '0.0.0.0')

            #signup_code = SignupCode.objects.create_signup_code(user, code, ipaddr)

            #signup_code.send_signup_email()

            # content = {'email': email, 'first_name': first_name,
            #     'last_name': last_name, 'phone': phone, 'address': address, 'credit': 0}
            content = {'email': email}
            return Response(content, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#
# class SignupVerify(APIView):
#     permission_classes = (AllowAny,)
#
#     def get(self, request, format=None):
#         code = request.GET.get('code', '')
#         verified = SignupCode.objects.set_user_is_verified(code)
#
#         if verified:
#             try:
#                 signup_code = SignupCode.objects.get(code=code)
#                 signup_code.delete()
#             except SignupCode.DoesNotExist:
#                 pass
#             content = {'success': 'User verified.'}
#             return Response(content, status=status.HTTP_200_OK)
#         else:
#             content = {'detail': 'Unable to verify user.'}
#             return Response(content, status=status.HTTP_400_BAD_REQUEST)


class Login(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.DATA)

        if serializer.is_valid():
            email = serializer.data['email']
            password = serializer.data['password']
            user = authenticate(email=email, password=password)

            if user:
                if not user.is_verified:
                    user.is_verified = True

                if user.is_active:
                    # remove previous token (handle multiple login)
                    # dealt with the previous token == new token
                    tokens = Token.objects.filter(user=user)
                    previous_keys = []
                    for token in tokens:
                        previous_keys += [token.key]
                        token.delete()

                    while True:
                        token = Token.objects.create(user=user)
                        for previous_key in previous_keys:
                            if token.key == previous_key:
                                token.delete()
                                break
                        if token.key is not None:
                            user.last_login = timezone.now()
                            user.save()

                            return Response({'token': token.key},
                                status=status.HTTP_200_OK)

                else:
                    content = {'detail': 'User account not active.'}
                    return Response(content, 
                        status=status.HTTP_401_UNAUTHORIZED)
            else:
                content = {'detail': 
                    'Unable to login with provided credentials.'}
                return Response(content, status=status.HTTP_401_UNAUTHORIZED)

        else:
            return Response(serializer.errors, 
                status=status.HTTP_400_BAD_REQUEST)

#
# class Logout(APIView):
#     permission_classes = (IsAuthenticated,)
#
#     def get(self, request, format=None):
#         """
#         Remove all auth tokens owned by request.user.
#         """
#         tokens = Token.objects.filter(user=request.user)
#         for token in tokens:
#             token.delete()
#         content = {'success': 'User logged out.'}
#         return Response(content, status=status.HTTP_200_OK)


class PasswordReset(APIView):
    permission_classes = (AllowAny,)
    serializer_class = TempPasswordSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.DATA)

        if serializer.is_valid():
            email = serializer.data['email']
            # password = serializer.data['password']
            # first_name = serializer.data['first_name']
            # last_name = serializer.data['last_name']
            # phone = serializer.data['phone']
            # address = serializer.data['address']

            user = get_user_model().objects.get(email=email)
            if not user or not user.is_verified:
                content = {'detail':
                    'User with this Email does not exist.'}
                return Response(content, status=status.HTTP_400_BAD_REQUEST)

            tokens = Token.objects.filter(user=user)
            for token in tokens:
                token.delete()

            code = _generate_temp_password()

            # Set user fields provided
            user.set_password(code)
            #user.is_verified = True
            # user.first_name = first_name
            # user.last_name = last_name
            # user.phone = phone
            # user.address = address
            user.save()

            user.send_email('temp_password_email', code)

            # Create and associate signup code
            #ipaddr = self.request.META.get('REMOTE_ADDR', '0.0.0.0')

            #signup_code = SignupCode.objects.create_signup_code(user, code, ipaddr)

            #signup_code.send_signup_email()

            # content = {'email': email, 'first_name': first_name,
            #     'last_name': last_name, 'phone': phone, 'address': address, 'credit': 0}
            content = {'email': email}
            return Response(content, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class PasswordChange(APIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = PasswordChangeSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.DATA)

        if serializer.is_valid():

            old_password = serializer.data['old_password']
            new_password = serializer.data['new_password']

            request_user = request.user
            email = request_user.email

            if email == request.GET.get('user', ''):

                user = authenticate(email=email, password=old_password)

                if user:
                    user.set_password(new_password)
                    user.save()
                    content = {'success': 'password changed.'}
                    return Response(content, status=status.HTTP_200_OK)
                else:
                    content = {'error': 'provided password is incorrect.'}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)
            else:
                # token & user are not matched, the way to avoid multiple users having same tokens
                return Response({'error': 'invalid request'}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)

# class PasswordReset(APIView):
#     permission_classes = (AllowAny,)
#     serializer_class = PasswordResetSerializer
#
#     def post(self, request, format=None):
#         serializer = self.serializer_class(data=request.DATA)
#
#         if serializer.is_valid():
#             email = serializer.data['email']
#
#             try:
#                 user = get_user_model().objects.get(email=email)
#                 if user.is_verified and user.is_active:
#                     password_reset_code = \
#                         PasswordResetCode.objects.create_reset_code(user)
#                     password_reset_code.send_password_reset_email()
#                     content = {'email': email}
#                     return Response(content, status=status.HTTP_201_CREATED)
#
#             except get_user_model().DoesNotExist:
#                 pass
#
#             # Since this is AllowAny, don't give away error.
#             content = {'detail': 'Password reset not allowed.'}
#             return Response(content, status=status.HTTP_400_BAD_REQUEST)
#
#         else:
#             return Response(serializer.errors,
#                 status=status.HTTP_400_BAD_REQUEST)

#
# class PasswordResetVerify(APIView):
#     permission_classes = (AllowAny,)
#
#     def get(self, request, format=None):
#         code = request.GET.get('code', '')
#
#         try:
#             password_reset_code = PasswordResetCode.objects.get(code=code)
#             content = {'success': 'User verified.'}
#             return Response(content, status=status.HTTP_200_OK)
#         except PasswordResetCode.DoesNotExist:
#             content = {'detail': 'Unable to verify user.'}
#             return Response(content, status=status.HTTP_400_BAD_REQUEST)
#
#
# class PasswordResetVerified(APIView):
#     permission_classes = (AllowAny,)
#     serializer_class = PasswordResetVerifiedSerializer
#
#     def post(self, request, format=None):
#         serializer = self.serializer_class(data=request.DATA)
#
#         if serializer.is_valid():
#             code = serializer.data['code']
#             password = serializer.data['password']
#
#             try:
#                 password_reset_code = PasswordResetCode.objects.get(code=code)
#                 password_reset_code.user.set_password(password)
#                 password_reset_code.user.save()
#                 content = {'success': 'Password reset.'}
#                 return Response(content, status=status.HTTP_200_OK)
#             except PasswordResetCode.DoesNotExist:
#                 content = {'detail': 'Unable to verify user.'}
#                 return Response(content, status=status.HTTP_400_BAD_REQUEST)
#
#         else:
#             return Response(serializer.errors,
#                 status=status.HTTP_400_BAD_REQUEST)